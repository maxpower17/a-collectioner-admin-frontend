import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exposures',
  templateUrl: './exposures.component.html',
  styleUrls: ['./exposures.component.css']
})
export class ExposuresComponent implements OnInit {

  exposures = [];
  archived = [];

  constructor() { }

  ngOnInit() {
  }

}
