import { Component, OnInit } from '@angular/core';
import {StorageService} from '../../services/storage.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  token: string;

  constructor(
    private storageService: StorageService,
    private router: Router
  ) {
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.token = this.storageService.token;
      }
    });
  }

  ngOnInit() {
  }

}
