import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import {ProfilesComponent} from './components/profiles/profiles.component';
import {ExhibitsComponent} from './components/exhibits/exhibits.component';
import {ExposuresComponent} from './components/exposures/exposures.component';

const routes: Routes = [
  { path: 'auth', component: AuthComponent },
  { path: 'profiles', component: ProfilesComponent },
  { path: 'exhibit', component: ExhibitsComponent },
  { path: 'exposure', component: ExposuresComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
