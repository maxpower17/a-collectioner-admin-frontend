import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth.service';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getUsers(): Observable<any> {
    return this.httpClient.get<any>(
      `${environment.urlApi}api/account`,
      { headers: new HttpHeaders('Access-Control-Allow-Origin: *')}
    );
  }

  archiveUser(userId: number): Observable<any> {
    return this.httpClient.post<void>(
      `${environment.urlApi}api/account/archive`,
      { userId }
    );
  }

  deleteUser(id: number): Observable<any> {
    return this.httpClient.delete<any>(
      `${environment.urlApi}api/account/${id}`,
      {}
    );
  }
}
