import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {StorageService} from './storage.service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpClient: HttpClient,
    private storageService: StorageService
  ) { }

  signIn(email: string, password: string)
    : Observable<{token: string}> {
    return this.httpClient.post<{ token: string }>(
      `${environment.urlApi}api/account/login`,
      { email, password }
    );
  }

}
