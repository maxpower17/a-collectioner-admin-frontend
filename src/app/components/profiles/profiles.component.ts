import { Component, OnInit } from '@angular/core';
import {AccountsService} from '../../services/accounts.service';
import {StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.css']
})
export class ProfilesComponent implements OnInit {

  accounts = [];
  archived = [];

  constructor(
    private accountsService: AccountsService
  ) { }

  ngOnInit() {
    this.accountsService.getUsers().subscribe(
      response => { this.accounts = response; }
    );
  }

}
