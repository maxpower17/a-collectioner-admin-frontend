import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {StorageService} from '../../services/storage.service';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import * as HttpStatus from 'http-status-codes';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loaded = false;
  errorMessage = '';

  form: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });


  constructor(
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService
  ) { }

  ngOnInit() {
  }

  hasError(control: string, error: string): boolean {
    return (this.form.dirty || this.form.touched)
      && this.form.invalid
      && this.form.get(control).hasError(error);
  }

  onSubmit(): void {
    this.loaded = true;

    this.authService
      .signIn(
        this.form.controls.email.value,
        this.form.controls.password.value
      )
      .subscribe(
        (response) => {
          this.storageService.token = response.token;
          this.router.navigate(['/profiles']);
        },
        (error) => {
          this.loaded = false;
          this.errorMessage = HttpStatus.getStatusText(error.status);
        },
        () => {
          this.loaded = false;
        }
      );
  }

}
