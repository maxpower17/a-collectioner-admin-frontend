import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AuthComponent} from './components/auth/auth.component';
import {NavComponent} from './components/nav/nav.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatTabsModule,
  MatToolbarModule,
  MatProgressSpinnerModule
} from '@angular/material';
import {ProfilePreviewComponent} from './components/profile-preview/profile-preview.component';
import {ProfilesComponent} from './components/profiles/profiles.component';
import {HttpClientModule} from '@angular/common/http';
import { ExhibitsComponent } from './components/exhibits/exhibits.component';
import { ExposuresComponent } from './components/exposures/exposures.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ErrorMessageComponent } from './components/error-message/error-message.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    NavComponent,
    ProfilePreviewComponent,
    ProfilesComponent,
    ExhibitsComponent,
    ExposuresComponent,
    ErrorMessageComponent
  ],
  imports: [
    BrowserModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonToggleModule,
    MatButtonModule,
    AppRoutingModule,
    MatListModule,
    MatTabsModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
