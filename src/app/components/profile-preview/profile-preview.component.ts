import {Component, Input, OnInit} from '@angular/core';
import {AccountsService} from '../../services/accounts.service';

@Component({
  selector: 'app-profile-preview',
  templateUrl: './profile-preview.component.html',
  styleUrls: ['./profile-preview.component.css']
})
export class ProfilePreviewComponent implements OnInit {

  @Input() id: number;
  @Input() image: string;
  @Input() name: string;
  @Input() type: string;

  constructor(
    private accountsService: AccountsService
  ) { }

  ngOnInit() {
  }

  onArchive() {
    alert(this.id);
  }

  onDelete() {
    alert(this.id);
    this.accountsService.deleteUser(this.id)
      .subscribe(
        () => { },
        () => { },
        () => {}
      );
  }

}
