import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  public get token(): string {
    return localStorage.getItem('token');
  }

  public set token(value: string) {
    localStorage.setItem('token', value);
  }

  public get loggedId(): number {
    return (+localStorage.getItem('loggedId'));
  }

  public set loggedId(value: number) {
    localStorage.setItem('loggedId', value.toString());
  }

  clearStorage(): void {
    localStorage.clear();
  }
  
}

